"""
Test assertions for automated testing.
"""
import re

def assert_equal(actual, expected, actual_name):
    """
    Asserts that arguments 'actual' and 'expected' are equal.
    Returns False if values are not equal or a TypeError is encountered

    :param actual: value under test
    :param expected: expected value for the value under test
    :param actual_name: name of actual, used in reporting result
    :return: tuple (result, message) where result is True if values are equal False if not
    """
    try:
        if actual == expected:
            return True, "{} equals '{}'".format(actual_name, expected)
        else:
            return False, "{} equals '{}' NOT '{}' as expected".format(actual_name, actual, expected)
    except TypeError:
        return False, "{} does NOT equal {} because they are of incomparable types".format(actual_name, expected)


def assert_true(actual, actual_name):
    """
    Asserts that arguments 'actual' is True
    Assertion will fail if 'actual' is not a boolean

    :param actual: value under test
    :param actual: name of actual, used in reporting result
    :return: tuple (result, message) where result is True if 'actual' is True
    """
    if actual is True:
        return True, "{} is True".format(actual_name)
    else:
        return False, "{} is NOT True".format(actual_name)

def assert_string_contains(actual, expected, actual_name):
    """
    Asserts that given string 'actual' contains an expected substring
    Assertion will fail if actual is not a string-like object

    :param actual: string
    :param expected: expected substring contained in actual. Supports regex pattern matching
    :param actual_name: name of actual, used in reporting
    :return: tuple (result, message) where result is True expected substring is contained in actual
    """

    if not isinstance(actual, str):
        return False, "{} is not a string".format(actual_name)

    if re.search(expected, actual):
        return True, "'{}' is contained in {}".format(expected, actual_name)
    else:
        return False, "'{}' is NOT contained in {}".format(expected, actual_name)

