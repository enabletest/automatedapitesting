"""
Acceptance test for category 6237
"""
import json
import urllib.request


class BaseAPITest:
    """
    Baseclass for API tests. url_under_test can be set upon test instantiation
    or defined as a class variable when the test is written.
    """
    def __init__(self, url_under_test=None):

        if url_under_test:
            self.url_under_test = url_under_test
        self.data = None
        self.results = []

    def setup(self):
        """
        Retrieve JSON data
        """
        with urllib.request.urlopen(self.url_under_test) as url:
            self.data = json.loads(url.read().decode())

    def run(self):
        """
        This should be implemented by the subclassing test.
        """
        raise NotImplemented

    def teardown(self):
        """
        Clean up anything that needs cleaning up and determine overall result of test
        :return: String that reports the result of the test
        :return: List of tuples containing: (test result as boolean, test output message)
        """

        if len(self.results) == 0:
            return "No assertions made, this test is not useful"

        pass_results = len([result for result in self.results if result[0] is True])

        overall_result = "PASS" if pass_results == len(self.results) else "FAIL"
        result_message = "Overall result - {}, {}/{}".format(overall_result, pass_results, len(self.results))

        # In a more developed test framework these results would also be uploaded to a database here
        # so that a record of test is persisted. However for the purposes of the exercise we just
        # return them.

        return result_message, self.results
