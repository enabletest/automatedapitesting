"""
Test acceptance criteria for category 6327
"""

from resources.test_base import BaseAPITest
from resources.test_assertions import assert_equal, assert_true, assert_string_contains


class tmsandboxAPITest(BaseAPITest):
    """
    Test class for category 6237

    Test URL: https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false

    Acceptance criteria:
    - Name = "Carbon credits"
    - CanRelist = true
    - The Promotions element with Name = "Gallery" has a Description that contains the text "2x larger image"
    """

    url_under_test = r"https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false"

    def run(self):
        # Acceptance tests
        name_val = self.data["Name"]
        self.results.append(assert_equal(name_val, "Carbon credits", "Name"))

        can_relist = self.data["CanRelist"]
        self.results.append(assert_true(can_relist, "CanRelist"))

        promotions = self.data["Promotions"]
        gallery_promotion_description = [promotion for promotion in promotions if
                                         promotion["Name"] == "Gallery"][0]["Description"]
        self.results.append(assert_string_contains(gallery_promotion_description, "2x larger image",
                                                   "Gallery promotion description"))


if __name__ == "__main__":
    test = tmsandboxAPITest()
    test.setup()
    test.run()
    result_message, results = test.teardown()
    for result in results:
        print(result)
    print(result_message)
