# README #

This repository is for a test exercise as part of interview process for test engineer role.

v1.0


### Test details ###

Test is run by invoking the test file tmsandbox_api_test.py in a console. For example on Windows without python set on path:
<pre>
C:\Users\Jared\AppData\Local\Programs\Python\Python37\python.exe C:/Users/Jared/PycharmProjects/master/tests/tmsandbox_api_test.py
</pre>

Test is written for python 3 (developed on 3.7)

